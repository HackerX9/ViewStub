package com.palfund.viewstub;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.FrameLayout;
import android.widget.ImageView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private FrameLayout mContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContainer = (FrameLayout) findViewById(R.id.container);
        final SharedPreferences userCenter = getSharedPreferences("UserCenter", MODE_PRIVATE);
        boolean isFirst = userCenter.getBoolean("isFirst", true);
        if (isFirst) {
            //第一次加载Splash
            inflateSplash(userCenter);
        } else {
            //加载主界面
            inflateMain();
        }
    }

    private void inflateSplash(final SharedPreferences userCenter) {
        ViewStub viewStubSplash = findViewById(R.id.viewStub_splash);
        final ViewPager splash = (ViewPager) viewStubSplash.inflate();
        ArrayList<ImageView> list = new ArrayList<>();
        ImageView imageView;
        int[] images = {R.drawable.p6, R.drawable.p7, R.drawable.p9, R.drawable.p10, R.drawable
                .p11};
        for (int i = 0; i < 5; i++) {
            imageView = new ImageView(this);
            imageView.setImageResource(images[i]);
            list.add(imageView);
        }
        splash.setAdapter(new BannerPagerAdapter(list));
        final float[] downX = new float[1];
        splash.setOnTouchListener(new View.OnTouchListener() {


            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //滑动到最后一个
                if (splash.getCurrentItem() == 4) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            downX[0] = event.getX();
                            Log.i("MainActivity", "---onTouch()--->downX   " + downX[0]);
                            break;
                        case MotionEvent.ACTION_MOVE:
                            float moveX = event.getX();
                            Log.i("MainActivity", "---onTouch()--->moveX   " + (downX[0] - moveX));
                            if (downX[0] - moveX > 200) {
                                //必须设置,否则onTouch()方法会继续调用
                                splash.setEnabled(false);
                                //滑动距离大于200px,加载主界面
                                inflateMain();
                                Log.i("MainActivity", "---onTouch()--->" + "切换");
                                //利用removeView销毁View释放内存mContainer.removeView(splash);
                                //splash.setVisibility(View.GONE);
                                mContainer.removeView(splash);
                                userCenter.edit().putBoolean("isFirst", false).commit();
                            }
                            break;
                        case MotionEvent.ACTION_UP:

                            break;
                        default:
                            break;
                    }
                }
                return false;
            }
        });
    }

    private void inflateMain() {
        ViewStub viewStubMain = findViewById(R.id.viewStub_main);
        View view = viewStubMain.inflate();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(Menu.NONE, 1, 1, "reset");
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                SharedPreferences userCenter = getSharedPreferences("UserCenter", MODE_PRIVATE);
                userCenter.edit().putBoolean("isFirst", true).apply();
                break;
            case 2:

                break;
            case 3:

                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
