package com.palfund.viewstub;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * @author noone on 2017/11/8.
 *         真正的速度是看不见的 !
 *         Today is today , we will go !
 */

public class BannerPagerAdapter extends PagerAdapter {
    private ArrayList<ImageView> mList;

    public BannerPagerAdapter(ArrayList<ImageView> list) {
        mList = list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView = mList.get(position);
        container.addView(imageView);
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
